

#include <stdio.h>
#include <math.h>
#include <CImg.h>
#include <time.h>
#include <errno.h>
#include <immintrin.h>
using namespace cimg_library;

// Repetitions to set algorithm time in the interval [5,10]
uint repetitions = 10;

// Data type for image components
// FIXME: Change this type according to your group assignment
typedef float data_t;

const char* SOURCE_IMG      = "bailarina.bmp";
const char* DESTINATION_IMG = "bailarina2.bmp";

//Image 2
const char* SOURCE2_IMG      = "background_V.bmp";

int main() {
	// Open file and object initialization
	CImg<data_t> srcImage(SOURCE_IMG);
	CImg<data_t> srcImage2(SOURCE2_IMG);

	data_t *pRsrc, *pGsrc, *pBsrc; // Pointers to the R, G and B components
	data_t* pRsrc2, * pGsrc2, * pBsrc2; // Pointers to the R, G and B components of image2
	data_t *pRdest, *pGdest, *pBdest;
	data_t *pDstImage; // Pointer to the new image pixels
	uint width, height; // Width and height of the image
	uint width2, height2; // Width and height of image2
	uint nComp; // Number of image components
	struct timespec tStart, tEnd;
	double dElapsedTimeS;

	//SIMD variables
	__m256 constant255 = _mm256_set1_ps(255);
	__m256 constant256 = _mm256_set1_ps(256);
	__m256 constant1 = _mm256_set1_ps(1);
	__m256 component1, component2;
	__m256 op1,op2,op3,op4;
	__m256 lastOp;
	



	/***************************************************
	 * TODO: Variables initialization.
	 *   - Prepare variables for the algorithm
	 *   - This is not included in the benchmark time
	 */

	//Image 1
	srcImage.display(); // Displays the source image
	width  = srcImage.width(); // Getting information from the source image
	height = srcImage.height();
	nComp  = srcImage.spectrum(); // source image number of components
				// Common values for spectrum (number of image components):
				//  B&W images = 1
				//	Normal color images = 3 (RGB)
				//  Special color images = 4 (RGB and alpha/transparency channel)

	//Image 2
	srcImage2.display(); // Displays the image2
	width2  = srcImage2.width(); // Getting information from the background image
	height2 = srcImage2.height();
	
	
	if (width != width2 || height != height2) { // Check images dimensions
			printf("Dimensions of images do not match");
			exit(-1);

		}

	// Allocate memory space for destination image components
	pDstImage = (data_t *) malloc (width * height * nComp * sizeof(data_t));
	if (pDstImage == NULL) {
		perror("Allocating destination image");
		exit(-2);
	}

	// Pointers to the componet arrays of the source image
	pRsrc = srcImage.data(); // pRcomp points to the R component array
	pGsrc = pRsrc + height * width; // pGcomp points to the G component array
	pBsrc = pGsrc + height * width; // pBcomp points to B component array
	
	// Pointers to the component arrays of the image 2
	pRsrc2 = srcImage2.data(); // pRcomp points to the R component array
	pGsrc2 = pRsrc2 + height2 * width2; // pGcomp points to the G component array
	pBsrc2 = pGsrc2 + height2 * width2; // pBcomp points to B component array

	// Pointers to the RGB arrays of the destination image
	pRdest = pDstImage;
	pGdest = pRdest + height * width;
	pBdest = pGdest + height * width;


	/***********************************************
	 * TODO: Algorithm start.
	 *   - Measure initial time
	 */
if(clock_gettime(CLOCK_REALTIME, &tStart)!=0)
	{
	perror("clock_gettime");
		exit(EXIT_FAILURE);
	}


	for (uint j=0; j<repetitions; j++){
			for (uint i = 0; i < width * height; i++) {

				// Red component 
				// Calculate algorithm step by step with SIMD instructions 
				component1 = _mm256_loadu_ps((pRsrc+i));
				component2 = _mm256_loadu_ps((pRsrc2+i));
				op1 = _mm256_sub_ps(constant255,component2);
				op2 = _mm256_mul_ps(constant256,op1); 
				op3 = _mm256_add_ps(component1,constant1);
				op4 = _mm256_div_ps(op2,op3);
				lastOp = _mm256_sub_ps(constant255,op4);

				_mm256_storeu_ps((pRdest+i),lastOp); //Save result of algorithm in red component array

				// Green component
				// Calculate algorithm step by step with SIMD instructions 
				component1 = _mm256_loadu_ps((pGsrc+i));
				component2 = _mm256_loadu_ps((pGsrc2+i));
				op1 = _mm256_sub_ps(constant255,component2);
				op2 = _mm256_mul_ps(constant256,op1); 
				op3 = _mm256_add_ps(component1,constant1);
				op4 = _mm256_div_ps(op2,op3);
				lastOp = _mm256_sub_ps(constant255,op4);

				_mm256_storeu_ps((pGdest+i),lastOp); //Save result of algorithm in green component array

				// Blue component
				// Calculate algorithm step by step with SIMD instructions 
				component1 = _mm256_loadu_ps((pBsrc+i));
				component2 = _mm256_loadu_ps((pBsrc2+i));
				op1 = _mm256_sub_ps(constant255,component2);
				op2 = _mm256_mul_ps(constant256,op1); 
				op3 = _mm256_add_ps(component1,constant1);
				op4 = _mm256_div_ps(op2,op3);
				lastOp = _mm256_sub_ps(constant255,op4);

				_mm256_storeu_ps((pBdest+i),lastOp); //Save result of algorithm in blue component array
				
				
				
			}
	}
	
				

	/***********************************************
	 * TODO: End of the algorithm.
	 *   - Measure the end time
	 *   - Calculate the elapsed time
	 */

		if(clock_gettime(CLOCK_REALTIME, &tEnd)!=0)
	{
		perror("clock_gettime");
		exit(EXIT_FAILURE);
	}
	dElapsedTimeS = (tEnd.tv_sec - tStart.tv_sec);
	dElapsedTimeS += (tEnd.tv_nsec - tStart.tv_nsec) / 1e+9;
	printf("Elapsed time    : %f s.\n", dElapsedTimeS);

	// Create a new image object with the calculated pixels
	// In case of normal color images use nComp=3,
	// In case of B/W images use nComp=1.
	CImg<data_t> dstImage(pDstImage, width, height, 1, nComp);

	// Cut image between {0,255}
	dstImage.cut(0,255);

	// Store destination image in disk
	dstImage.save(DESTINATION_IMG); 

	// Display destination image
	dstImage.display();
	
	// Free memory
	_mm_free(pDstImage);

	return 0;
}
